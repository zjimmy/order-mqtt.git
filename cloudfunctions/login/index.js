// 云函数模板
// 部署：在 cloud-functions/login 文件夹右击选择 “上传并部署”

/**
 * 这个示例将经自动鉴权过的小程序用户 openid 返回给小程序端
 * 
 * event 参数包含
 * - 小程序端调用传入的 data
 * - 经过微信鉴权直接可信的用户唯一标识 openid 
 * 
 */
const cloud = require('wx-server-sdk')
cloud.init({})
const db = cloud.database()
const _ = db.command
exports.main = async (event, context) => {
  console.log(event)
  console.log(context)
  // let userid;
  let openid = event.userInfo.openId;
 let findUnser = await db.collection('user').where({
   openid: _.eq(openid)
  })
    .get({
      success: function (res) {
        cossole.log('+++++:',res)
        resolove(res)
      }
   })
  
  if (findUnser.data.length){
    return {
      openid: event.userInfo.openId,
      userInfo: findUnser.data[0]
    }
  }


  let res = await db.collection('user').add({
    // data 字段表示需新增的 JSON 数据
    data: {
      // _id: 'todo-identifiant-aleatoire', // 可选自定义 _id，在此处场景下用数据库自动分配的就可以了
      openid: event.userInfo.openId,
      time: Date.parse(new Date()),
      info: {
        avatarUrl: '',
        city: '',
        country: '',
        gender: 1,
        language: '',
        nickName: '',
        province: '',
        
      }
    },
    success: function (res) {
      // res 是一个对象，其中有 _id 字段标记刚创建的记录的 id
        resolove(res)
    }
  })
  // 可执行其他自定义逻辑
  // console.log 的内容可以在云开发云函数调用日志查看
  let canFindUnser = await db.collection('user').where({
    openid: _.eq(openid)
   })
     .get({
       success: function (res) {
         resolove(res)
       }
    })
   
   if (canFindUnser.data.length) {
     return {
       openid: event.userInfo.openId,
       userInfo: canFindUnser.data[0]
     }
   }
  
}

