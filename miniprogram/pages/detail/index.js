// pages/userConsole/userConsole.js
Page({

  data: {
    soundSource : '',
    bgImage:"../../images/bg.jpeg"
  },

  onLoad: function (opt) {
    if(opt.soundId){
    this.setData({
      soundSource: opt.soundId,
    
    })
  }else{
    console.log('no sound')
  }
  if(opt.imgId){
    wx.cloud.downloadFile({
      fileID: opt.imgId,
      success: res => {
        // 返回临时文件路径
        console.log(res.tempFilePath)
        this.setData({
          bgImage:res.tempFilePath
        })
       
      },
      fail: console.error
    })
  }
  
},
listen:async function(){
  await wx.cloud.downloadFile({
    fileID: this.data.soundSource,
    success: res => {
      // 返回临时文件路径
      console.log(res.tempFilePath)

      wx.playVoice({
        filePath: res.tempFilePath,
        complete: () => {
         
            console.log('playSound|play sound end')
        
        }
    })
    },
    fail: console.error
  })
 
},
})